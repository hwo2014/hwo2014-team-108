class StatusObject(object):
    game_ticks = 0

    angle = 0
    speed = 0

    piece_index = 0
    prev_piece_index = -1

    lap = 0
    prev_lap = -1

    in_piece_distance = 0
    prev_in_piece_distance = 0

    pos = (0, 0)
    prev_pos = (0, 0)

    width = 0
    length = 0
    flag_pos = 0

    track = None
