from piece import *


class Track(object):
    pieces = []

    def __init__(self, race_data):
        track_data = race_data['track']
        pieces_data = track_data['pieces']

        self.addPieces(pieces_data)

    def addPieces(self, pieces_data):
        new_pos = (0, 0)
        new_angle = 0

        for piece_data in pieces_data:
            new_piece = Piece(piece_data, new_pos, new_angle)

            old_pos = new_pos  # FIXME: old_pos wordt niet gebruikt
            new_pos = new_piece.end_pos
            new_angle = new_piece.end_angle

            self.pieces.append(new_piece)

    def getPiece(self, piece_index):
        return self.pieces[piece_index]
