import json
import socket
import sys
import math

from piece import *
from track import *
from status import *

debug = False

if socket.gethostname() == "Willems-MacBook-Pro.local":
    # Exporteer de baan naar een png file op Willem's laptop
    import Image
    import ImageDraw

    img = Image.new("RGB", (2000, 2000), "white")
    draw = ImageDraw.Draw(img)
    debug = True


class HyperionBot(object):
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

        self.status = StatusObject()

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def send_throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_init(self, data):
        race_data = data['race']
        cars_data = race_data['cars'][0]

        self.status.width     = cars_data['dimensions']['width']
        self.status.length    = cars_data['dimensions']['length']
        self.status.flag_post = cars_data['dimensions']['guideFlagPosition']

        self.status.track = Track(race_data)

        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def update_history(self):
        self.status.prev_in_piece_distance = self.status.in_piece_distance
        self.status.prev_lap               = self.status.lap
        self.status.prev_piece_index       = self.status.piece_index
        self.status.prev_pos               = self.status.pos

        self.status.game_ticks += 1

    def parse_status_data(self, data):
        self.update_history()

        self.status.angle             = data['angle']
        self.status.in_piece_distance = data['piecePosition']['inPieceDistance']
        self.status.piece_index       = data['piecePosition']['pieceIndex']
        self.status.lap               = data['piecePosition']['lap']

        piece = self.status.track.getPiece(self.status.piece_index)
        self.status.pos = piece.getPos(self.status.in_piece_distance)
        self.status.progress = piece.getProgress(self.status.in_piece_distance)

        dx = self.status.pos[0] - self.status.prev_pos[0]
        dy = self.status.pos[1] - self.status.prev_pos[1]

        self.status.speed = math.sqrt(dx**2 + dy**2)

    def on_car_positions(self, data):
        self.parse_status_data(data)

        print self.status.piece_index, self.status.progress * 100, self.status.speed, self.status.pos

        if debug:
            draw.point((self.status.pos[0] + 1000, 1000 - self.status.pos[1]), "blue")

        self.send_throttle(0.25)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join':          self.on_join,
            'gameStart':     self.on_game_start,
            'carPositions':  self.on_car_positions,
            'crash':         self.on_crash,
            'gameEnd':       self.on_game_end,
            'error':         self.on_error,
            'gameInit':      self.on_init,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']

            if type(data) is list and len(data) == 1:
                data = data[0]
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

if __name__ == "__main__":
    try:
        if len(sys.argv) != 5:
            print("Usage: ./run host port botname botkey")
        else:
            host, port, name, key = sys.argv[1:5]
            print("Connecting with parameters:")
            print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((host, int(port)))
            bot = HyperionBot(s, name, key)
            bot.run()
    finally:
        print "Finalizing"
        if debug == True:
            img.save("track.png", "PNG")
