import math


class Piece(object):
    switch = False

    start_pos = (0, 0)
    end_pos   = (0, 0)

    start_angle = 0
    end_angle = 0

    bend_angle = 0
    bend_radius = 0

    length = 0

    piece_type = None

    def __init__(self, piece_data, start_pos, start_angle):
        self.start_pos = start_pos
        self.start_angle = start_angle

        if 'switch' in piece_data:
            self.switch = True

        if 'angle' in piece_data:
            self.bend(piece_data)
            self.piece_type = "bend"
        else:
            self.straight(piece_data)
            self.piece_type = "straight"

    def straight(self, piece_data):
        piece_length = piece_data['length']

        delta_x = math.cos(self.start_angle) * piece_length
        delta_y = math.sin(self.start_angle) * piece_length

        self.end_angle = self.start_angle
        self.length = piece_length
        self.end_pos = (self.start_pos[0] + delta_x, self.start_pos[1] + delta_y)

    def bend(self, piece_data):
        piece_angle = math.radians(piece_data['angle'])
        piece_radius = piece_data['radius']
        piece_length = piece_angle * piece_radius

        x = piece_radius * math.sin(math.fabs(piece_angle))
        y = piece_radius * (1 - math.cos(math.fabs(piece_angle)))

        straight_length = math.sqrt(x**2 + y**2)
        straight_angle = piece_angle / 2

        fake_angle = self.start_angle - straight_angle

        delta_x = math.cos(fake_angle) * straight_length
        delta_y = math.sin(fake_angle) * straight_length

        self.end_pos = (self.start_pos[0] + delta_x, self.start_pos[1] + delta_y)
        self.end_angle = self.start_angle - piece_angle

        self.length = piece_length
        self.bend_angle = piece_angle
        self.bend_radius = piece_radius

    def getPos(self, in_piece_distance):
        if self.piece_type == "straight":
            delta_x = in_piece_distance * math.cos(self.start_angle)
            delta_y = in_piece_distance * math.sin(self.start_angle)
        else:
            angle = self.bend_angle * (in_piece_distance / self.length)

            x = math.sin(angle) * self.bend_radius
            y = self.bend_radius * (1 - math.cos(angle))

            if self.bend_angle < 0:
                y *= -1.0

            fake_length = math.sqrt(x**2 + y**2)

            fake_angle = self.start_angle - math.atan(y / x)

            delta_x = math.cos(fake_angle) * fake_length
            delta_y = math.sin(fake_angle) * fake_length

        pos = (self.start_pos[0] + delta_x, self.start_pos[1] + delta_y)

        return pos

    def getProgress(self, in_piece_distance):
        return in_piece_distance / self.length
